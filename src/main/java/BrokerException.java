/**
 * BrokerException class for handling errors
 *
 * @author Meg Zhang
 *
 */

public class BrokerException extends Exception {
    public BrokerException(String msg){
        super(msg);
    }
}
